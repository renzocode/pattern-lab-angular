import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtomicDesignLibComponent } from './atomic-design-lib.component';

describe('AtomicDesignLibComponent', () => {
  let component: AtomicDesignLibComponent;
  let fixture: ComponentFixture<AtomicDesignLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtomicDesignLibComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtomicDesignLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
