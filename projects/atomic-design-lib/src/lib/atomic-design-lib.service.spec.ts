import { TestBed } from '@angular/core/testing';

import { AtomicDesignLibService } from './atomic-design-lib.service';

describe('AtomicDesignLibService', () => {
  let service: AtomicDesignLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AtomicDesignLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
