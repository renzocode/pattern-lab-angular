import { NgModule } from '@angular/core';
import { AtomsModule } from './atoms/atoms.module';
import { MoleculesModule } from './molecules/molecules.module';

@NgModule({
  declarations: [

  ],
  imports: [
    AtomsModule,
    MoleculesModule
  ],
  exports: [
    AtomsModule,
    MoleculesModule
  ]
})
export class AtomicDesignLibModule {
  constructor() {
    console.log('Atomic design lib');
  }
}
