import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlidersComponent } from './sliders/sliders.component';

const EXTERNAL_MODULE = [
]
@NgModule({
  declarations: [
    SlidersComponent
  ],
  imports: [
    CommonModule,
    ...EXTERNAL_MODULE
  ],
  exports: [
    SlidersComponent
  ]
})
export class MoleculesModule {
  constructor() {
    console.log('Atomic design Molecule');
  }
}
