import { Component, Input, TemplateRef, OnInit } from '@angular/core';

@Component({
  selector: 'lib-sliders',
  templateUrl: './sliders.component.html',
  styleUrls: ['./sliders.component.scss']
})
export class SlidersComponent implements OnInit {

  @Input() rowData : any[];
  @Input() colsTemplate: TemplateRef<any>[];
  @Input() headings: string[];
  
  constructor() { }

  ngOnInit(): void {
  }

}
