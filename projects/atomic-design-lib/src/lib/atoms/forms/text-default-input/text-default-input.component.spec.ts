import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextDefaultInputComponent } from './text-default-input.component';

describe('TextDefaultInputComponent', () => {
  let component: TextDefaultInputComponent;
  let fixture: ComponentFixture<TextDefaultInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
