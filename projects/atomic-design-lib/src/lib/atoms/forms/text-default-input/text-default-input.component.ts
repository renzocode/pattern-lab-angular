import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, forwardRef, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ETYPES } from '../../config/buttons.config';

@Component({
  selector: 'lib-text-default-input',
  templateUrl: './text-default-input.component.html',
  styleUrls: ['./text-default-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextDefaultInputComponent),
      multi: true
    }
  ]
})
export class TextDefaultInputComponent implements OnInit, ControlValueAccessor {
  
  @ViewChild('zipEl', { static: true }) zipEl: ElementRef;
  @Input() label = 'text input label';
  @Input() size = 'm'; // 's', 'm', 'l'
  @Input() marginBottom = '';
  @Input() customClass = '';
  @Input() hasErrorMessage = false;
  @Input() errorMessage = '';
  @Input() width = '';
  @Input() maxLength = 100;
  @Input() placeholder = '';
  @Input() icon = '';
  @Input() type: ETYPES = ETYPES.DEFAULT_B;
  @Input() isRequired = false;
  @Input() typeOfInput = 'text';
  @Input() isDisabled = true;
  @Input() labelIsDisabled = false;

  public value = '';
  
  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  constructor(
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
    private cdr:ChangeDetectorRef
  ) {
    
   }

  ngOnInit(): void {
    this.cdr.detectChanges();
  }

  writeValue(value: any): void {
    console.log('text...');
    console.log(value);
    if (value === '') {
      this._renderer.setProperty(this.zipEl.nativeElement, 'value', value);
    } else {
      this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    console.log(isDisabled);
    this.isDisabled = isDisabled;
  }

  public changeInput(target: HTMLInputElement): void {
    const value = target.value;
    this.onChange(value);
    this.onTouch(true);
  }
}
