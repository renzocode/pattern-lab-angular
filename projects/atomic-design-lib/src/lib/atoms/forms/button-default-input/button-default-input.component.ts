import { 
  ChangeDetectionStrategy,
  Component, ElementRef, EventEmitter, forwardRef,
  Injector, Input, OnDestroy, OnInit,
  Output, Renderer2, ViewChild } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormControl,
  FormControlDirective,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors, Validator } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EPOSITIONS, ETYPES } from '../../config/buttons.config';

@Component({
  selector: 'lib-button-default-input',
  templateUrl: './button-default-input.component.html',
  styleUrls: ['./button-default-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ButtonDefaultInputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      multi: true,
      useExisting: ButtonDefaultInputComponent
    }
  ]
})
export class ButtonDefaultInputComponent implements OnInit, ControlValueAccessor, Validator,  OnDestroy {
  
  @ViewChild('zipEl', { static: true }) zipEl: ElementRef;
  public isDisabled = false;
  public customButton: boolean;
  formControlDirective: FormControlDirective;
  @Input() type: ETYPES = ETYPES.DEFAULT_B;
  @Input() isOutline = false;
  @Input() position: EPOSITIONS = EPOSITIONS.CENTER;
  @Input() isFull = false;
  @Input() isContainerFull = false;
  @Input() customWidth = '';
  @Input() typeOfInput = 'button';
  @Input() maxLength = 100;
  @Input() placeholder = '';
  @Input() customValue: string = '';
  @Input() text = '';

  @Output() onclick = new EventEmitter();

  private subscriptions: Subscription[] = [];

  formControl: FormControl;
  formDirective: FormControlDirective;

  constructor(  
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
    private injector: Injector,
  ) { 
    console.log('button instance');
  }

  ngOnInit(): void {
  
  }

  validate(control: AbstractControl): ValidationErrors {
    return ;
  }
  registerOnValidatorChange?(fn: () => void): void {
  }

  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  ngOnDestroy(): void {
    console.log('button default input destroy');
    this.subscriptions.forEach( subscription => subscription.unsubscribe());
    // this._elementRef.nativeElement.removeControl(this);
    if (this.formDirective) {
      //this.formDirective.removeControl(this);
    }
  }

  writeValue(value: any): void {
    // this.formControlDirective.valueAccessor.writeValue(value);
    console.log('get value sss');
    console.log(value);
    // this.onChange(value);
    this.customButton = value == this.customValue ? true: false;
    if (value === '') {
      // this._renderer.setProperty(this.zipEl.nativeElement, 'value', value);
    } else {
      // this.value = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    console.log(isDisabled);
    this.isDisabled = isDisabled;
  }

  public buttonClick(): void {
    if (!this.isDisabled) {
      if (!this.customButton ) {
        this.onclick.emit(this.customValue);
      } else {
        this.onclick.emit('');
      }
    }
  }
}
