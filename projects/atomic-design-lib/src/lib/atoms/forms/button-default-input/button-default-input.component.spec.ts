import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonDefaultInputComponent } from './button-default-input.component';

describe('ButtonDefaultInputComponent', () => {
  let component: ButtonDefaultInputComponent;
  let fixture: ComponentFixture<ButtonDefaultInputComponent>;

  // the first testing
  it('Our first Jasmine test 1', () => {
    expect(true).toBe(true);
  });

  it('2 + 2 equals 4', ()=> {
    expect(2+2).toBe(4);
  });

  // second testing
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('Should have a valid contructor', ()=> {
    component.text = 'Data';
    expect(component.text).toEqual('Data');
  });
  
   afterEach(() => {
     component = null;
   });
});
