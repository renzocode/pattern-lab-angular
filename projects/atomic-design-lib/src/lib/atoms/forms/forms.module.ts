import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  TextareaDefaultInputComponent } from './textarea-default-input/textarea-default-input.component';
import {
  SelectDefaultInputComponent } from './select-default-input/select-default-input.component';
import {
  TextDefaultInputComponent } from './text-default-input/text-default-input.component';
import { ButtonDefaultInputComponent } from './button-default-input/button-default-input.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    TextareaDefaultInputComponent,
    SelectDefaultInputComponent,
    TextDefaultInputComponent,
    ButtonDefaultInputComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    TextareaDefaultInputComponent,
    SelectDefaultInputComponent,
    TextDefaultInputComponent,
    ButtonDefaultInputComponent
  ]
})
export class FormsModule { }
