import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TextareaDefaultInputComponent } from './textarea-default-input.component';

describe('TextareaDefaultInputComponent', () => {
  let component: TextareaDefaultInputComponent;
  let fixture: ComponentFixture<TextareaDefaultInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TextareaDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TextareaDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
