import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxDefaultInputComponent } from './checkbox-default-input.component';

describe('CheckboxDefaultInputComponent', () => {
  let component: CheckboxDefaultInputComponent;
  let fixture: ComponentFixture<CheckboxDefaultInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckboxDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
