import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectDefaultInputComponent } from './select-default-input.component';

describe('SelectDefaultInputComponent', () => {
  let component: SelectDefaultInputComponent;
  let fixture: ComponentFixture<SelectDefaultInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
