import { Component, ElementRef, forwardRef, Input, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { B_TYPES, ETYPES } from '../../config/buttons.config';
import { ICustomSelectOption } from '../../config/form.config';

const INITIAL_OPTION = { value: -1, text: '' };

@Component({
  selector: 'lib-select-default-input',
  templateUrl: './select-default-input.component.html',
  styleUrls: ['./select-default-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectDefaultInputComponent),
      multi: true
    }
  ]
})
export class SelectDefaultInputComponent implements OnInit, ControlValueAccessor, OnDestroy {
  
  @ViewChild('zipEl', { static: true }) zipEl: ElementRef;
  @Input() isDisabled = false;
  @Input() type: ETYPES = ETYPES.DEFAULT_B;
  @Input() isRequired = false;
  @Input() label = 'text input label';
  @Input() marginBottom = '';
  @Input() labelIsDisabled = false;
  @Input() hasErrorMessage = false;
  @Input() errorMessage = 'error';
  @Input() value = '';
  @Input() customWidth = '';
  @Input() options: ICustomSelectOption[] = [
    { value: -1, text: 'Seleccionar' }
  ];

  public selectedOption: ICustomSelectOption = INITIAL_OPTION as ICustomSelectOption;
  public data: any;

  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  constructor(
    private _renderer: Renderer2,
    private _elementRef: ElementRef,
  ) {}

  ngOnDestroy(): void {}

  ngOnInit() {
    const isValidType = B_TYPES.includes(this.type);
    this.type = isValidType ? this.type : ETYPES.DEFAULT_B;
  }

  writeValue(option: ICustomSelectOption): void {
    this.onTouch(true);
    console.log('text...');
    console.log(option);
    this.options.forEach(element => {
      if (element.value ==  option.value) {
        this._renderer.setProperty(this.zipEl.nativeElement, 'value', option.value);
        this.selectedOption = option;
      }
    });
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  public changeOption(target: HTMLSelectElement) {
    const selectedValue = Number(target.value);
    const selectedOption = this.options.find(option => option.value === selectedValue);
    this.onChange(selectedOption ? selectedOption : INITIAL_OPTION);
    this.onTouch(true);
  }
}
