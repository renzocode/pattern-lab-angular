import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckBoxDefaultInputComponent } from './check-box-default-input.component';

describe('CheckBoxDefaultInputComponent', () => {
  let component: CheckBoxDefaultInputComponent;
  let fixture: ComponentFixture<CheckBoxDefaultInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckBoxDefaultInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckBoxDefaultInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
