import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'lib-check-box-default-input',
  templateUrl: './check-box-default-input.component.html',
  styleUrls: ['./check-box-default-input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckBoxDefaultInputComponent),
      multi: true
    }
  ]
})
export class CheckBoxDefaultInputComponent implements OnInit, ControlValueAccessor {
  public isDisabled = false;
  public checked = false;

  onChange = (_: any) => { };
  onTouch = (_: any) => { };

  constructor() { }

  ngOnInit(): void {
  }

  writeValue(value: any): void {
    if (typeof value === 'boolean') {
      this.checked = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  public clickInput(value: HTMLInputElement): void {
    this.checked = value.checked;
    this.onChange(this.checked);
    this.onTouch(this.checked);
  }
}
