export interface ICustomSelectOption {
  value: number;
  text: string;
}

export interface ICustomNumber {
  value: number;
}