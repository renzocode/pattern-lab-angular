export enum ETYPES {
  DEFAULT_B = 'default-color',
  SELECT_COLOR_1_B = 'select-color-1',
  RESET_COLOR_1_A = 'reset-color-1',
  INPUT_COLOR_1_A = 'input-color-1'
}

export const B_TYPES = [
  ETYPES.DEFAULT_B,
  ETYPES.SELECT_COLOR_1_B,
  ETYPES.RESET_COLOR_1_A,
  ETYPES.INPUT_COLOR_1_A
];

export enum EPOSITIONS {
  LEFT = 'left',
  RIGTH = 'rigth',
  CENTER = 'center',
  TOP = 'top',
  BOTTOM = 'bottom'
}

export const B_POSITIONS = [EPOSITIONS.LEFT, EPOSITIONS.RIGTH, EPOSITIONS.CENTER];

export const PIXELES_REGEXP = /^[0-9]{1,}px$/;

export interface INgStyle {
  [ key: string ]: any;
}
