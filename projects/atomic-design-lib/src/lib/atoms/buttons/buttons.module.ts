import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetDefaultButtonComponent } from './reset-default-button/reset-default-button.component';

@NgModule({
  declarations: [
    ResetDefaultButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ResetDefaultButtonComponent
  ]
})
export class ButtonsModule { 
  constructor() {
    console.log('Button module instance')
  }
}
