import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetDefaultButtonComponent } from './reset-default-button.component';

describe('ResetDefaultButtonComponent', () => {
  let component: ResetDefaultButtonComponent;
  let fixture: ComponentFixture<ResetDefaultButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetDefaultButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetDefaultButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
