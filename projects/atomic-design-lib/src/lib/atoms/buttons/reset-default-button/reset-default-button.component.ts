import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { B_POSITIONS, B_TYPES, EPOSITIONS, ETYPES, INgStyle, PIXELES_REGEXP } from '../../config/buttons.config';

@Component({
  selector: 'lib-reset-default-button',
  templateUrl: './reset-default-button.component.html',
  styleUrls: ['./reset-default-button.component.scss']
})
export class ResetDefaultButtonComponent implements OnInit {

  @Input() type: ETYPES = ETYPES.DEFAULT_B;
  @Input() isOutline = false;
  @Input() position: EPOSITIONS = EPOSITIONS.CENTER;
  @Input() isFull = false;
  @Input() isContainerFull = false;
  @Input() isDisabled = false;
  @Input() customWidth = '';

  public customAttributes: INgStyle = {};

  @Output() onclick = new EventEmitter();

  constructor() { 
    console.log('Button instance 1')
  }

  ngOnInit(): void {
    // reseting if is necessary
    const isValidType = B_TYPES.includes(this.type);
    this.type = isValidType ? this.type : ETYPES.DEFAULT_B;
    const isValidPosition = B_POSITIONS.includes(this.position);
    this.position = isValidPosition ? this.position : EPOSITIONS.CENTER;
    // const isValidCustomWidth = PIXELES_REGEXP.test(this.customWidth);
    // this.customWidth = isValidCustomWidth ? this.customWidth : '';
    this.settingCustomAttribites();
  }

  private settingCustomAttribites(): void {
    if (this.customWidth) {
      this.customAttributes.width = this.customWidth;
    }
  }

  public buttonClick(): void {
    if (!this.isDisabled) {
      this.onclick.emit();
    }
  }
}
