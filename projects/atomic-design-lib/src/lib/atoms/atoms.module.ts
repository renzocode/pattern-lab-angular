import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdvertisingModule } from './advertising/advertising.module';
import { AnimationModule } from './animation/animation.module';
import { BlocksModule } from './blocks/blocks.module';
import { ButtonsModule } from './buttons/buttons.module';
import { ColorsModule } from './colors/colors.module';
import { FormsModule } from './forms/forms.module';
import { GlobalElementsModule } from './global-elements/global-elements.module';
import { HeadingsModule } from './headings/headings.module';
import { IconsModule } from './icons/icons.module';
import { ImageTypesModule } from './image-types/image-types.module';
import { InteractiveComponentsModule } from './interactive-components/interactive-components.module';
import { ListsModule } from './lists/lists.module';
import { MediaModule } from './media/media.module';
import { MessagingModule } from './messaging/messaging.module';
import { NavigationModule } from './navigation/navigation.module';
import { ThirdPartyComponentsModule } from './third-party-components/third-party-components.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdvertisingModule,
    AnimationModule,
    BlocksModule,
    FormsModule,
    ButtonsModule,
    ColorsModule,
    GlobalElementsModule,
    HeadingsModule,
    IconsModule,
    ImageTypesModule,
    InteractiveComponentsModule,
    ListsModule,
    MediaModule,
    MessagingModule,
    NavigationModule,
    ThirdPartyComponentsModule
  ],
  exports: [
    AdvertisingModule,
    AnimationModule,
    BlocksModule,
    ButtonsModule,
    ColorsModule,
    FormsModule,
    GlobalElementsModule,
    HeadingsModule,
    IconsModule,
    ImageTypesModule,
    InteractiveComponentsModule,
    ListsModule,
    MediaModule,
    MessagingModule,
    NavigationModule,
    ThirdPartyComponentsModule
  ]
})
export class AtomsModule { 
  constructor() {
    console.log('Atomic Design Atoms');
  }
}
