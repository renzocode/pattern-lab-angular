import { TestBed } from '@angular/core/testing';

import { DomainDriveDesignService } from './domain-drive-design.service';

describe('DomainDriveDesignService', () => {
  let service: DomainDriveDesignService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DomainDriveDesignService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
