import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DomainDriveDesignComponent } from './domain-drive-design.component';

describe('DomainDriveDesignComponent', () => {
  let component: DomainDriveDesignComponent;
  let fixture: ComponentFixture<DomainDriveDesignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DomainDriveDesignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DomainDriveDesignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
