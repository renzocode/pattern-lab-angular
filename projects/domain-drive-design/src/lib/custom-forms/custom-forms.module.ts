import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from './forms/forms.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class CustomFormsModule { }
