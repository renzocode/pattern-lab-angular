import { TestBed } from '@angular/core/testing';

import { PrimaryDefaultFormService } from './primary-default-form.service';

describe('PrimaryDefaultFormService', () => {
  let service: PrimaryDefaultFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrimaryDefaultFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
