import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ButtonInputControl } from '../../common/controls/button-default-input.control';

@Injectable({
  providedIn: 'root'
})
export class ArrayButtonsFormsService {

  public form: FormGroup = new FormGroup({});

  private buttonConditional1Value = new ButtonInputControl();
  private buttonConditional2Value = new ButtonInputControl();

  private arrayControl = [ 
   this.buttonConditional1Value,  
   this.buttonConditional2Value,
  ];

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      buttonGroup1: this.formBuilder.array([]),
    });
   
    this.arrayControl.forEach(element => this.buttonGroup1Control
      .push(this.formBuilder.group(
        {
          buttonConditional1: element
        }
      )
    ));
    console.log('Array Button Forms');
   }

  public set buttonGroup1Control(checkBoxValue: FormArray)  {
    this.form.controls['buttonGroup1']
      .setValue(checkBoxValue.value,
        {
          onlySelf: true,
          emitModelToViewChange: true,
          emitEvent: true
        }
      );
  }

  public get buttonGroup1Control(): FormArray {
    return this.form.get('buttonGroup1') as FormArray;
  }

  // Button Group1 Control

  // ButtonS1 Control
  public set buttonS1Control(buttonValue: ButtonInputControl) {
    this.buttonGroup1Control['controls'][0]
    .get('buttonConditional1')
    .setValue(buttonValue.value, 
      {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
    

  }

  public get buttonS1Control(): ButtonInputControl {
    return this.buttonGroup1Control['controls'][0].get('buttonConditional1') as ButtonInputControl;
  }

  public disableButtonS1Control() {
    this.buttonS1Control.disable();
  }

  public enableButtonS1Control() {
    this.buttonS1Control.enable();
  }

  public resetButtonS1Control() {
    this.buttonS1Control = {value: ''} as ButtonInputControl;
    this.buttonS1Control.markAsPristine();
    this.buttonS1Control.markAsUntouched();
    this.buttonS1Control.updateValueAndValidity({onlySelf: true});
  }

  // ButtonS2 Control
  public set buttonS2Control(buttonValue: ButtonInputControl) {
    this.buttonGroup1Control['controls'][1].get('buttonConditional1')
      .setValue(
        buttonValue.value,
        {
          onlySelf: true,
          emitModelToViewChange: true,
          emitEvent: false
        }
      );
  }

  public get buttonS2Control(): ButtonInputControl {
    return this.buttonGroup1Control['controls'][1].get('buttonConditional1') as ButtonInputControl;
  }

  public disableButtonS2Control() {
    this.buttonS2Control.disable();
  }

  public enableButtonS2Control() {
    this.buttonS2Control.enable();
  }

  public resetButtonS2Control() {
    this.buttonS2Control = {value: ''} as ButtonInputControl;
    this.buttonS2Control.markAsPristine();
    this.buttonS2Control.markAsUntouched();
    this.buttonS2Control.updateValueAndValidity({onlySelf: true});
  }

  public get formValues() {
    return { 
      buttonConditional1:  this.buttonS1Control.value,
      buttonConditional2:  this.buttonS2Control.value,
    }
  }

  public resetForm() {
    this.resetButtonS1Control();
    this.resetButtonS2Control();
  }
}
