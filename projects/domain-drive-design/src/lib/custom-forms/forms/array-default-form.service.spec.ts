import { TestBed } from '@angular/core/testing';

import { ArrayDefaultFormService } from './array-default-form.service';

describe('ArrayDefaultFormService', () => {
  let service: ArrayDefaultFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArrayDefaultFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
