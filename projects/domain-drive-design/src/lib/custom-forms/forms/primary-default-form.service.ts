import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CustomControl } from '../../common/controls/input-default.control';
import { CustomSelectControl } from '../../common/controls/select-default.control';

@Injectable({
  providedIn: 'root'
})
export class PrimaryDefaultFormService {

  public form: FormGroup = new FormGroup({});

  private fullFirst1NameValue = new CustomControl();
  private fullFirst2NameValue = new CustomControl();
  private fullLast1NameValue = new CustomSelectControl();
  private fullLast2NameValue = new CustomSelectControl();

  constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      fullFirst1Name: this.fullFirst1NameValue,
      fullFirst2Name: this.fullFirst2NameValue,
      fullLast1Name: this.fullLast1NameValue,
      fullLast2Name: this.fullLast2NameValue,
    });
  }

  // First1name control

  public set fullFirst1NameControl(fullFirst1NameValue: CustomControl) {
    this.form.controls['fullFirst1Name'].setValue(
      fullFirst1NameValue.value,
      {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
  }

  public get fullFirst1NameControl(): CustomControl {
    return this.form.get('fullFirst1Name') as CustomControl;
  }

  public disableFullFirst1NameControl(): void {
    this.fullFirst1NameControl.disable({onlySelf: true});
  }

  public resetFullFirst1NameControl(): void {
    this.fullFirst1NameControl  = {value: ''} as CustomControl;
    this.fullFirst1NameControl.markAsPristine();
    this.fullFirst1NameControl.markAsUntouched();
    this.fullFirst1NameControl.updateValueAndValidity({onlySelf: true});
  }

  // First2name control

  public set fullFirst2NameControl(fullFirst2NameValue: CustomControl) {
    this.form.controls['fullFirst2Name'].setValue(
      fullFirst2NameValue.value,
      {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
  }

  public get fullFirst2NameControl(): CustomControl {
    return this.form.get('fullFirst2Name') as CustomControl;
  }

  public disableFullFirst2NameControl() {
    this.fullFirst2NameControl.disable({onlySelf: true});
  }

  public resetFullFirst2NameControl() {
    this.fullFirst2NameControl = { value: ''} as CustomControl;
    this.fullFirst2NameControl.markAsPristine();
    this.fullFirst2NameControl.markAsUntouched();
    this.fullFirst2NameControl.updateValueAndValidity({onlySelf: true});
  }

  // Last1Name control

  public set fullLast1NameControl(fullLast1NameValue: CustomSelectControl) {
    this.form.controls['fullLast1Name'].setValue(
      fullLast1NameValue.value,
      {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
  }

  public get fullLast1NameControl(): CustomSelectControl {
    return this.form.get('fullLast1Name') as CustomSelectControl;
  }

  public errorFullLast1NameControl() {
    return this.form.getError('fullLast1Name');
  }

  public disableFullLast1NameControl() {
    this.fullLast1NameControl.disable();
  }

  public resetFullLast1NameControl() {
    this.fullLast1NameControl = { value: {value: -1, text:  'Seleccionar'} } as CustomSelectControl;
    this.fullLast1NameControl.markAsPristine();
    this.fullLast1NameControl.markAsUntouched();
    this.fullLast1NameControl.updateValueAndValidity({onlySelf: true});
  }

   // Last2Name control

   public set fullLast2NameControl(fullLast2NameValue: CustomSelectControl) {
    this.form.controls['fullLast2Name'].setValue(
      fullLast2NameValue.value,
       {
        onlySelf: true,
        emitModelToViewChange: true,
        emitEvent: true
      }
    );
  }

  public get fullLast2NameControl(): CustomSelectControl {
    return this.form.get('fullLast2Name') as CustomSelectControl;
  }

  public disableFullLast2NameControl() {
    this.fullLast2NameControl.disable();
  }

  public resetFullLast2NameControl() {
    this.fullLast2NameControl ={ value: {value: -1, text:  'Seleccionar'} }  as CustomSelectControl;
    this.fullLast2NameControl.markAsPristine();
    this.fullLast2NameControl.markAsUntouched();
    this.fullLast2NameControl.updateValueAndValidity({onlySelf: true});
  }


  public resetForm() {
    this.resetFullFirst1NameControl();
    this.resetFullFirst2NameControl();
    this.resetFullLast1NameControl();
    this.resetFullLast2NameControl();
  }

}
