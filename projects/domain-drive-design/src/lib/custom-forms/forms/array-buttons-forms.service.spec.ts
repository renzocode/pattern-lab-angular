import { TestBed } from '@angular/core/testing';

import { ArrayButtonsFormsService } from './array-buttons-forms.service';

describe('ArrayButtonsFormsService', () => {
  let service: ArrayButtonsFormsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArrayButtonsFormsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
