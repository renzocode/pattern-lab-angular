import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimaryDefaultFormService } from './primary-default-form.service';
import { ArrayButtonsFormsService } from './array-buttons-forms.service';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule
  ],
  providers: [
    PrimaryDefaultFormService,
    ArrayButtonsFormsService
  ]
})
export class FormsModule { }
