import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CheckControl } from '../../common/controls/checkbox-default.control';
import { ArrayDefaultControl } from '../../common/controls/array-default.control';

@Injectable({
  providedIn: 'root'
})
export class ArrayDefaultFormService {

  public form: FormGroup = new FormGroup({});

  private question1Value = new CheckControl();
  private question2Value = new CheckControl();
  private question3Value = new CheckControl();
  private question4Value = new CheckControl();

  private checkBoxValue = new ArrayDefaultControl([
    this.question1Value,
    this.question2Value,
    this.question3Value,
    this.question4Value
  ]);

 constructor(
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      checkboxes: this.checkBoxValue
    });
  }

  public set checkboxesControl(descriptionValue: ArrayDefaultControl)  {
    this.form.controls['checkboxes'].setValue(descriptionValue.value);
  }

  public get checkboxesControl(): ArrayDefaultControl {
    return this.form.get('checkboxes') as ArrayDefaultControl;
  }

  public disableCelNumberControl(): void {
    this.checkboxesControl.disable();
  }

  public resetCelNumberControl(): void {
    this.checkboxesControl = { value: [null, null, null, null]} as ArrayDefaultControl;
    this.checkboxesControl.markAsPristine();
    this.checkboxesControl.markAsUntouched();
    this.checkboxesControl.updateValueAndValidity();
  }
}
