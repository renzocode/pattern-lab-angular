import { NgModule } from '@angular/core';
import { CustomFormsModule } from './custom-forms/custom-forms.module';
import { DomainDriveDesignComponent } from './domain-drive-design.component';



@NgModule({
  declarations: [
    DomainDriveDesignComponent
  ],
  imports: [
    CustomFormsModule
  ],
  exports: [
    DomainDriveDesignComponent
  ]
})
export class DomainDriveDesignModule { }
