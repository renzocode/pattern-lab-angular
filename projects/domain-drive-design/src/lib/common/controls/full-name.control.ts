import { AbstractControl, FormControl, Validators } from '@angular/forms';

export class FullNameControl extends FormControl {

  public value: string = '';

  constructor() {
    super('');
    this.settingNameValidators();
    this.setValue(CUSTOM_INPUT_DEFAULT_VALUE);
  }

  private settingNameValidators(): void {
    this.setValidators([
      Validators.required,
      Validators.pattern('^([A-Za-z\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s ]*)$'),
    ]);
  }
}

export const CUSTOM_INPUT_DEFAULT_VALUE = '' as string;

