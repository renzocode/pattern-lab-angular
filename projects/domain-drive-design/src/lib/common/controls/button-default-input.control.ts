import { AbstractControl, FormControl, Validators } from '@angular/forms';

export class ButtonInputControl extends FormControl {

  public value: string;

  constructor() {
    super('');
    this.settingNameValidators();
    // this.setValue(CUSTOM_INPUT_DEFAULT_VALUE);
  }

  private settingNameValidators(): void {
    this.setValidators([
      Validators.required,
      this.customButtonValidator
    ]);
  }

  private customButtonValidator(control: AbstractControl) {
    const controlValue = control.value;
    return controlValue.value != '' ? null : { selectValidator: true };
  }
}


export const CUSTOM_INPUT_DEFAULT_VALUE = '';