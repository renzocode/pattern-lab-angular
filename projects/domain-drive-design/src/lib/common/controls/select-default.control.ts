import { 
  FormControl,
  Validators,
  AbstractControl } from '@angular/forms';


export interface ICustomSelectOption {
    value: number;
    text: string;
}
  
export class CustomSelectControl extends FormControl {
  
  public value: ICustomSelectOption = {} as ICustomSelectOption;
  
  constructor() {
    super(CUSTOM_SELECT_DEFAULT_VALUE);
    this.settingNameValidators();
    this.setValue(CUSTOM_SELECT_DEFAULT_VALUE);
  }
  
  private settingNameValidators(): void {
    this.setValidators([
      Validators.required,
      this.customSelectValidator
    ]);
  }
  
  public get selectErrorMessage(): string {
    return getErrorbyFormControl(this, CUSTOM_SELECT_ERROR_MESSAGES);
  }
  
  private customSelectValidator(control: AbstractControl) {
    const controlValue = control.value;
    return controlValue.value != -1 && controlValue.text != 'Seleccionar' ? null : { selectValidator: true };
  }
}

export function getErrorbyFormControl(formControl: FormControl, errorMessageArray: any ): string {
  if (getEnableErrorMessages(formControl)) {
    if (formControl.errors) {
      const errorKeys = Object.keys(formControl.errors);
      const currentKey = errorKeys[0];
      return errorMessageArray[currentKey];
    }
  }
  return '';
}

export function getEnableErrorMessages(formControl: FormControl): boolean {
  const value = formControl.invalid && ( formControl.touched || formControl.dirty );
  return value;
}

export const CUSTOM_SELECT_DEFAULT_VALUE = { value: -1, text: 'Seleccionar', disabled: true } as ICustomSelectOption;


export const CUSTOM_SELECT_ERROR_MESSAGES = {
  required: 'Campo requerido',
  selectValidator: 'Opción invalida',
};