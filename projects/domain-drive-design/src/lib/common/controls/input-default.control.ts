import { FormControl, Validators } from '@angular/forms';

export class CustomControl extends FormControl {

  public value: string;

  constructor() {
    super('');
    this.settingNameValidators();
    this.setValue(CUSTOM_INPUT_DEFAULT_VALUE);
  }

  private settingNameValidators(): void {
    this.setValidators([
      Validators.required,
    ]);
  }
}


export const CUSTOM_INPUT_DEFAULT_VALUE = '';
