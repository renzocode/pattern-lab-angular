import { AbstractControl, FormControl, Validators } from '@angular/forms';

export class TextAreaControl extends FormControl {

  public value: string = '';

  constructor() {
    super('');
    this.settingNameValidators();
    this.setValue(CUSTOM_INPUT_DEFAULT_VALUE);
  }

  private settingNameValidators(): void {
    this.setValidators([
      Validators.required,
      Validators.pattern('^([A-Za-z0-9\u00C0-\u00D6\u00D8-\u00f6\u00f8-\u00ff\s\ -.`"]*)$'),
    ]);
  }

  private customFullNameValidator(control: AbstractControl) {
    const controlValue = control.value;
    return Number(controlValue)  ? null : { selectValidator: false };
  }
}

export const CUSTOM_INPUT_DEFAULT_VALUE = '';

