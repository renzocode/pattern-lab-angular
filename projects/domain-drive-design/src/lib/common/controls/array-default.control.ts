import { FormArray, FormControl, Validators } from '@angular/forms';

export class ArrayDefaultControl extends FormArray {

    public value: FormControl[];

    constructor(arrayValue: FormControl[]) {
      super(arrayValue);
      this.setValue(arrayValue);
    }

    private settingNameValidators(): void {
      this.setValidators([
        Validators.required,
        Validators.requiredTrue
      ]);
    }
}

export const CUSTOM_ARRAY_DEFAULT_VALUE = [] as FormControl[];

