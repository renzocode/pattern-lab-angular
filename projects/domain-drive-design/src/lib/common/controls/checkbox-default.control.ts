import { FormControl, Validators } from '@angular/forms';

export class CheckControl extends FormControl {

    public value: boolean;

    constructor() {
      super(false);
      this.settingNameValidators();
      this.setValue(CUSTOM_CHECKBOX_DEFAULT_VALUE);
    }

    private settingNameValidators(): void {
      this.setValidators([
        Validators.required,
        Validators.requiredTrue
      ]);
    }
}

export const CUSTOM_CHECKBOX_DEFAULT_VALUE = false as boolean;

