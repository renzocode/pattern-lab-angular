/*
 * Public API Surface of domain-drive-design
 */

export * from './lib/domain-drive-design.service';
export * from './lib/domain-drive-design.component';
export * from './lib/domain-drive-design.module';
