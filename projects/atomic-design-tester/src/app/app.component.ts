import { Component } from '@angular/core';
import { CustomSelectControl } from 'projects/domain-drive-design/src/lib/common/controls/select-default.control';
import { PrimaryDefaultFormService } from 'projects/domain-drive-design/src/lib/custom-forms/forms/primary-default-form.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
}
