import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SecondaryViewTestComponent } from './secondary-view-test.component';

describe('SecondaryViewTestComponent', () => {
  let component: SecondaryViewTestComponent;
  let fixture: ComponentFixture<SecondaryViewTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SecondaryViewTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SecondaryViewTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
