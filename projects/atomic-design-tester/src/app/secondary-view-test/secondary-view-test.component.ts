import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ButtonInputControl } from 'projects/domain-drive-design/src/lib/common/controls/button-default-input.control';
import { ArrayButtonsFormsService } from 'projects/domain-drive-design/src/lib/custom-forms/forms/array-buttons-forms.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-secondary-view-test',
  templateUrl: './secondary-view-test.component.html',
  styleUrls: ['./secondary-view-test.component.scss']
})
export class SecondaryViewTestComponent implements OnInit, OnDestroy {

  @Input() hasErrorMessage = false;
  @Input() errorMessage = 'error';

  public text:any = '';
  private subscriptions: Subscription[] = [];
  public isDisabledGotoFordwardOneButton: boolean = false;

  constructor(
    public arrayButtonsForms: ArrayButtonsFormsService
  ) {
    console.log('secondary view');
  }

  ngOnInit(): void {
    const sub01 = this.arrayButtonsForms.buttonGroup1Control.valueChanges.subscribe(
      res => {
        console.log(res);
        let d  = this.arrayButtonsForms.form.valid;
        console.log('Form valid');
        console.log(d);
      }
    );
    this.subscriptions.push(sub01);

    const sub02 = this.arrayButtonsForms.form.valueChanges.subscribe(
      res => {
        console.log(res);
        let d  = this.arrayButtonsForms.form.valid;
        console.log('Form valid');
        console.log(d);
      }
    );
    this.subscriptions.push(sub02);

    const sub03 = this.arrayButtonsForms.buttonS1Control.valueChanges.subscribe(
      res => {
        console.log('Button 1');
        console.log(res);
      }
    );
    this.subscriptions.push(sub03);

    const sub04 = this.arrayButtonsForms.buttonS2Control.valueChanges.subscribe(
      res => {
        console.log('Button 2');
        console.log(res);
      }
    );
    this.subscriptions.push(sub04);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach( subscription => subscription.unsubscribe());
  }

  public buttonS1($event: string) {
    this.arrayButtonsForms.buttonS1Control = { value: $event} as ButtonInputControl;;
  }

  public buttonS2($event: string) {
    this.arrayButtonsForms.buttonS2Control = { value: $event} as ButtonInputControl;;
  }

  public reset() {
   this.arrayButtonsForms.resetForm();
  }

  public submit() {
    const { buttonConditional1, buttonConditional2 } = this.arrayButtonsForms.formValues;
    this.text = "conditional 1: " + buttonConditional1 + ", conditional 2: " + buttonConditional2;
  }

  public enable1() {
    this.arrayButtonsForms.enableButtonS1Control();
  }

  public disable1() {
    this.arrayButtonsForms.disableButtonS1Control();
  }

  public enable2() {
    this.arrayButtonsForms.enableButtonS2Control();
  }
  
  public disable2() {
    this.arrayButtonsForms.disableButtonS2Control();
  }
}
