import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryViewTestComponent } from './primary-view-test.component';

describe('PrimaryViewTestComponent', () => {
  let component: PrimaryViewTestComponent;
  let fixture: ComponentFixture<PrimaryViewTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrimaryViewTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryViewTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
