import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomSelectControl } from 'projects/domain-drive-design/src/lib/common/controls/select-default.control';
import { PrimaryDefaultFormService } from 'projects/domain-drive-design/src/lib/custom-forms/forms/primary-default-form.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-primary-view-test',
  templateUrl: './primary-view-test.component.html',
  styleUrls: ['./primary-view-test.component.scss']
})
export class PrimaryViewTestComponent implements OnInit, OnDestroy {

  public title: string = 'atomic-design-tester';
  public isDisabledGotoFordwardOneButton: boolean = false;
  private subscriptions: Subscription[] = [];

  public options = [
    {value: -1, text: 'Seleccionar'},
    {value: 1, text: 'Paris'},
    {value: 2, text: 'Belin'},
    {value: 3, text: 'Lima'}
  ];

  constructor(
    public primaryDefaultForm: PrimaryDefaultFormService,
    private router: Router,
  ) {
    console.log('primary view');
  }

  ngOnInit(): void {
    const sub01 = this.primaryDefaultForm.form.valueChanges.subscribe(
      res => {}
    );
    this.subscriptions.push(sub01);

    const sub02 = this.primaryDefaultForm.fullLast1NameControl.valueChanges
      .subscribe(res => {
        console.log(res);
      });
    this.subscriptions.push(sub02); 

    const sub03 = this.primaryDefaultForm.fullLast2NameControl.valueChanges
      .subscribe(res => {});
    this.subscriptions.push(sub03);    
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach( subscription => subscription.unsubscribe());
  }

  public reset() {
    this.primaryDefaultForm.resetForm();
  }

  public setUpSelect() {
    const data = { value:  {value: 2, text: 'Belin'}} as CustomSelectControl;
    this.primaryDefaultForm.fullLast1NameControl = data;
  }

  public goToResumen() {
    if (!this.primaryDefaultForm.form.valid) {
      this.router.navigate(['/resumen']);
    }
  }
}
