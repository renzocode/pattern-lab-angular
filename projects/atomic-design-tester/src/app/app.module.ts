import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AtomicDesignLibModule } from 'projects/atomic-design-lib/src/public-api';
import { DomainDriveDesignModule } from 'projects/domain-drive-design/src/public-api';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { PrimaryViewTestComponent } from './primary-view-test/primary-view-test.component';
import { SecondaryViewTestComponent } from './secondary-view-test/secondary-view-test.component';
import { ThirdViewTestComponent } from './third-view-test/third-view-test.component';


const EXTERNAL_MODULE = [
  FormsModule,
  ReactiveFormsModule,
]

@NgModule({
  declarations: [
    AppComponent,
    PrimaryViewTestComponent,
    SecondaryViewTestComponent,
    ThirdViewTestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AtomicDesignLibModule,
    DomainDriveDesignModule,
    ...EXTERNAL_MODULE
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    console.log('App modules')
  }
 }
