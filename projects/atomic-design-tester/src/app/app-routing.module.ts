import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrimaryViewTestComponent } from './primary-view-test/primary-view-test.component';
import { SecondaryViewTestComponent } from './secondary-view-test/secondary-view-test.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AtomicDesignLibModule } from 'projects/atomic-design-lib/src/public-api';
import { DomainDriveDesignModule } from 'projects/domain-drive-design/src/public-api';
import { ThirdViewTestComponent } from './third-view-test/third-view-test.component';

const EXTERNAL_MODULE = [
  FormsModule,
  ReactiveFormsModule,
]

const routes: Routes = [
  {
    path: '',
    component: PrimaryViewTestComponent,
    pathMatch: 'full'
  },
  {
    path: 'resumen',
    component: SecondaryViewTestComponent,
    pathMatch: 'full'
  },
  {
    path: 'slider',
    component: ThirdViewTestComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    AtomicDesignLibModule,
    DomainDriveDesignModule,
    ...EXTERNAL_MODULE
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
