import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-third-view-test',
  templateUrl: './third-view-test.component.html',
  styleUrls: ['./third-view-test.component.scss']
})
export class ThirdViewTestComponent implements OnInit {

  public employees = [
    {
        id: 1,
        name: 'Scott',
        designation: 'Dev',
        exp: 4
    },
    {
        id: 2,
        name: 'Billy',
        designation: 'QA',
        exp: 3
    },
    {
        id: 3,
        name: 'Frank',
        designation: 'Lead',
        exp: 8
    },
    {
        id: 4,
        name: 'Alan',
        designation: 'Manager',
        exp: 10
    },
  ];

  public headings = [
    'Id',
    'Name',
    'Designation',
    'Experience'
  ];

  @ViewChild('id', { static: true }) id: any;
  @ViewChild('name', { static: true }) name: any;
  @ViewChild('des', { static: true }) des: any;
  @ViewChild('exp', { static: true }) exp: any;

  cols: TemplateRef<any>[] = [];


  constructor() { }

  ngOnInit(): void {
    this.cols.push(this.id, this.name, this.des, this.exp);
  }

}
