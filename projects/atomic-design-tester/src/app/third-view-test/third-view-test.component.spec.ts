import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThirdViewTestComponent } from './third-view-test.component';

describe('ThirdViewTestComponent', () => {
  let component: ThirdViewTestComponent;
  let fixture: ComponentFixture<ThirdViewTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThirdViewTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThirdViewTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
